﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AhkComUtilities.UnitTests
{
    using AhkComUtilities;
    using System.IO;

    [TestClass]
    public class IniUtilityTests
    {
        private string GetTempIniPathNonExist()
        {
            var tmpIniPath = Path.Combine(Environment.GetEnvironmentVariable("TEMP"), "ini.ini");

            if (File.Exists(tmpIniPath))
            {
                File.Delete(tmpIniPath);
                System.Threading.Thread.Sleep(100);
            }

            return tmpIniPath;
        }

        [TestMethod]
        public void TotalTest()
        {
            var tmpIniPath = GetTempIniPathNonExist();

            var iniUtility = new IniUtility();
            iniUtility.Initialize(tmpIniPath);

            var iniValue = Guid.NewGuid().ToString();

            iniUtility.SetValue("s-tahir", "k-martin", iniValue);

            var v = iniUtility.GetValue("s-tahir", "k-martin");

            Console.WriteLine(iniValue);

            Assert.AreEqual(v, iniValue, "setting and getting a value should return the same value");
        }

        [TestMethod]
        public void EnternalEditingTest()
        {
            var tmpIniPath = GetTempIniPathNonExist();

            var iniUtility = new IniUtility();
            iniUtility.Initialize(tmpIniPath);

            var iniValue = Guid.NewGuid().ToString();

            iniUtility.SetValue("s-tahir", "k-martin", iniValue);

            string replacement = "eoiueemcncncc";

            var newText = File.ReadAllText(tmpIniPath).Replace(iniValue, replacement);
            File.WriteAllText(tmpIniPath, newText);

            System.Threading.Thread.Sleep(100);

            var val = iniUtility.GetValue("s-tahir", "k-martin");

            Assert.AreEqual(val, replacement, "after editing the value by hand, the value is reflected immediately");
        }

        [TestMethod]
        public void RemoveValueTest()
        {
            var tmpIniPath = GetTempIniPathNonExist();

            var iniSection = "s-tahir";
            var iniKey = "k-martin";
            var iniValue = Guid.NewGuid().ToString();

            bool fileTextContains(string s) => File.ReadAllText(tmpIniPath).Contains(s);

            {
                var iniUtility = new IniUtility(tmpIniPath);
                iniUtility.SetValue(iniSection, iniKey, iniValue);
                // make sure the file now has the value after setting it
                Assert.IsTrue(fileTextContains(iniValue), "file text must now contain the ini value");

                // make sure the object also returns the same value
                Assert.AreEqual(iniValue, iniUtility.GetValue(iniSection, iniKey), "also the object returns the new value regardless of it being persisted to the file");

                iniUtility.RemoveValue(iniSection, iniKey);

                // make sure the file no longer has the value after calling RemoveValue
                Assert.IsFalse(fileTextContains(iniValue), "file text should not contain the value after calling removevalue");

                // make sure that the object also has it removed.
                Assert.IsNull(iniUtility.GetValue(iniSection, iniKey), "the object should also make it disappear from its potentially internal collection");
            }
        }
    }
}
