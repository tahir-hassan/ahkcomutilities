﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace AhkComUtilities
{
    [Guid("0DB1CB26-F647-417E-ADB2-FCA0DC17618F")]
    // https://www.thomaslevesque.com/2013/05/21/an-easy-and-secure-way-to-store-a-password-using-data-protection-api/
    public interface IDataProtection
    {
        string Protect(string clearText, string optionalEntropy);
        string Unprotect(string encryptedText, string optionalEntropy);
    }
}
