param([string]$TargetPath)

$TargetDir = Split-Path $TargetPath
$TargetFileName = Split-Path $TargetPath -Leaf
$TargetTlbFileName = & {
   $ind = $TargetFileName.LastIndexOf(".dll")
   $TargetFileName.Substring(0, $ind) + ".tlb"
}

# IMPORTANT *** should be in project directory root (in accord with the post build event)
Set-Alias 'RegAsm' "$env:SystemRoot\Microsoft.NET\Framework64\v4.0.30319\RegAsm.exe"

Push-Location $TargetDir

if (Test-Path $TargetTlbFileName) {
    # unregister first
    RegAsm /unregister $TargetFileName /tlb:$TargetTlbFileName /codebase
    del $TargetTlbFileName

    Write-Host "Reg.ps1: Unregistered and deleted $TargetTlbFileName"
}

RegAsm $TargetFileName /tlb /codebase

Write-Host "Reg.ps1: registered the dll"

Pop-Location
