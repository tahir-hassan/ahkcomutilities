﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace AhkComUtilities
{
    [Guid("B069EC62-3B7B-4592-90CB-CE2C5ABE4076")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComSourceInterfaces(typeof(IIniUtility))]
    public class IniUtility : IIniUtility
    {
        public string Path { get; set; }
        private Ini Ini { get; set; }

        private DateTime LastEdited { get; set; } = DateTime.MinValue;

        private DateTime GetLastEdited()
        {
            return new FileInfo(Path).LastWriteTime;
        }

        private void UpdateLastEdited()
        {
            this.LastEdited = GetLastEdited();
        }

        private void EnsureIniCurrent()
        {
            if (File.Exists(Path))
            {
                var actualLastEdited = GetLastEdited();
                if (LastEdited < actualLastEdited)
                {
                    this.Ini = new Ini(Path);
                    LastEdited = actualLastEdited;
                }
            }
        }

        public void Initialize(string path)
        {
            this.Path = path;
            this.Ini = new Ini(path);

            if (File.Exists(path))
            {
                LastEdited = GetLastEdited();
            }
        }

        public IniUtility()
        {

        }

        public IniUtility(string path)
        {
            Initialize(path);
        }

        public string[] GetSections()
        {
            EnsureIniCurrent();
            return Ini.GetSections();
        }

        public string[] GetKeys(string section)
        {
            EnsureIniCurrent();
            return Ini.GetKeys(section);
        }

        public string GetRootValue(string key)
        {
            EnsureIniCurrent();
            return Ini.GetValue(key);
        }

        public string GetValue(string section, string key)
        {
            EnsureIniCurrent();
            return Ini.GetValue(key, section, null);
        }

        private void Edit(Action editingAction)
        {
            EnsureIniCurrent();
            editingAction();
            Ini.Save();
            UpdateLastEdited();
        }

        public void SetRootValue(string key, string value)
        {
            Edit(() => Ini.WriteValue(key, value));
        }

        public void SetValue(string section, string key, string value)
        {
            Edit(() => Ini.WriteValue(key, section, value));
        }

        public void RemoveValue(string iniSection, string iniKey)
        {
            Edit(() => Ini.RemoveValue(iniKey, iniSection));
        }

        public void RemoveRootValue(string iniKey)
        {
            Edit(() => Ini.RemoveValue(iniKey));
        }
    }
}
