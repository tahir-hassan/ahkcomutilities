﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace AhkComUtilities
{
    [Guid("F0B6B508-8469-40D2-BBD2-44F1F623D693")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComSourceInterfaces(typeof(IDataProtection))]
    public class DataProtection : IDataProtection
    {
        private string HandleNull(string s) => string.IsNullOrWhiteSpace(s) ? null : s.Trim();

        public string Protect(string clearText, string optionalEntropy)
        {
            return ThomasDataProtection.Protect(clearText, HandleNull(optionalEntropy));
        }

        public string Unprotect(string encryptedText, string optionalEntropy)
        {
            return ThomasDataProtection.Unprotect(encryptedText, HandleNull(optionalEntropy));
        }
    }
}
