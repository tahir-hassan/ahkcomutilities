﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AhkComUtilities
{
    public static class AhkDebugging
    {
        public static void DebugMsg(string msg)
        {
            Trace.WriteLine($"[AHK] {msg}");
        }
    }
}
