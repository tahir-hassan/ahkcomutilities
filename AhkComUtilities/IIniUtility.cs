﻿using System.Runtime.InteropServices;

namespace AhkComUtilities
{
    [Guid("C8E1D363-24EB-486C-A1DF-F3B519208C21")]
    public interface IIniUtility
    {
        void Initialize(string path);

        string Path { get; set; }

        string[] GetKeys(string section);
        string[] GetSections();
        string GetRootValue(string key);
        string GetValue(string section, string key);
        void SetRootValue(string key, string value);
        void SetValue(string section, string key, string value);

        void RemoveValue(string iniSection, string iniKey);
        void RemoveRootValue(string iniKey);
    }
}